package at.gleb.yachampion.detect_scr;

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import at.gleb.yachampion.DetectContract
import at.gleb.yachampion.R
import kotlinx.android.synthetic.main.fragment_detect.view.*
import kt_extensions.toast

/**
 * DialogFragment for Detect
 * Also it is a View that is involved to MVP, using DetectContract
 * @see DetectPresenter
 * <p>
 * @author Gleb Klimov,  info@gleb.at
 * @version 21.10.16
 */
class DetectFragment : DialogFragment(), DetectContract.View {

    override lateinit var presenter: DetectContract.Presenter


    override var detecting: Boolean = false
        set(value) {
            field = value

            if (value) {
//                v?.accelerometerButton?.isEnabled = false
                v?.accelerometerButton?.setText(R.string.autodetect_off)
                activity.toast(R.string.lets_go)


            } else {

//                v?.accelerometerButton?.isEnabled = true
                v?.accelerometerButton?.setText(R.string.autodetect_on)
            }

        }

    override var detectCount: Int = 0
        set(value) {
            field = value
            v?.countText?.setText(value.toString())
        }

    var v: View? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val i = activity.layoutInflater
        v = i.inflate(R.layout.fragment_detect, null)


        v?.accelerometerButton?.setOnClickListener {
            var res: Int = 0

            try {
                res = v?.countText?.text?.toString()?.toInt() ?: 0
            } catch(e: Throwable) {
            }


            if (!detecting) {

                presenter.startDetect(res)
            } else {
                presenter.stopDetect()
            }
        }

        val adb = AlertDialog.Builder(activity)
                .setTitle(R.string.detect_title)
                .setPositiveButton(R.string.ok, { dialog: DialogInterface?, which: Int? ->
                    val res: Int?

                    res = try {
                        v?.countText?.text?.toString()?.toInt()
                    } catch(e: Throwable) {
                        null
                    }


                    if (res != null) {
                        presenter.result(res)
                    } else {
                        //todo: show error message
                    }
                })
                .setNegativeButton(R.string.cancel, { dialog: DialogInterface?, which: Int? ->
                    dismiss()
                })
                .setView(v)


        presenter.start()

        return adb.create()
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        presenter.stopDetect()

    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        presenter.stopDetect()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.stopDetect()

    }


}
