package at.gleb.yachampion.detect_scr;

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import at.gleb.yachampion.DetectContract
import at.gleb.yachampion.models.registerNewNote
import kt_extensions.MyAppCompatActivity
import kt_extensions.d
import kt_extensions.dateCurrent
import java.util.*

/**
 * Presenter for Detect  that is involved to MVP, using DetectContract
 * <p>
 * @author Gleb Klimov,  info@gleb.at
 * @version 21.10.16
 */
class DetectPresenter(val view: DetectContract.View, val activity: MyAppCompatActivity)
: DetectContract.Presenter {

    val gravity = 9.8f

    var moveDown  = false // пометка о том, что мы начали движение вниз  -- начали приседать
    var moveUp = false // пометка о том, что мы начали движение вверх после того, как сделали движение вниз


    var countDetected = 0
        set(value) {
            field = value
            view.detectCount = value

        }


    override var selectedDate: Date = dateCurrent()
        set(value) {
            field = value
            d("set selected date")
        }



    init {
        view.presenter = this
    }


    override fun start() {

    }

    override fun startDetect(startCount:Int) {
        view.detecting  = true

        moveDown = false
        moveUp = false
        countDetected = startCount

        val sensorManager = activity.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorManager.registerListener(listener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL)

    }

    val listener: SensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

        }

        override fun onSensorChanged(event: SensorEvent?) {
            val zy: Float? = event?.values?.get(1)
//            d(zy.toString())

            if (zy != null) {

                if (zy > gravity + 0.1f) {
//                    d("вниз")

                    if (moveUp && moveDown) {
                        moveDown = false
                        moveUp = false
                        countDetected++
                        d("Приседаем вниз сразу после предыдущего приседания")

                        d("Мы присели $countDetected раз")


                    }

                    if (!moveDown && !moveUp) {
                        moveDown = true
                        d("Начали движение вниз")
                    }



                } else if (zy < gravity - 0.1f) {
//                    d("вверх")

                    if (moveDown && !moveUp) {
                        moveUp = true
                        d("Начали движение вверх после того, как приселт")

                    }
                } else {
//                    d("состояние покоя")

                    if (moveUp && moveDown) {
                        moveDown = false
                        moveUp = false
                        countDetected++

                        d("Состояние покоя, приседание окончено")


                        d("Мы присели $countDetected раз")
                    }
                }
            }
        }

    }


    override fun stopDetect() {

        val sensorManager = activity.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.unregisterListener(listener)

        view.detecting = false
    }


    override fun result(res: Int) {
        //d(res.toString())

        activity.registerNewNote(selectedDate, res)
    }



}