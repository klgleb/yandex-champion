package at.gleb.yachampion;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import at.gleb.yachampion.models.db.HelperFactory;

/**
 * Created by Gleb Klimov, info@gleb.at
 * on 22.10.16.
 */

public class MyApp extends Application {

    private static Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();

        Context appContext = getApplicationContext();

        MyApp.applicationContext = appContext;

        HelperFactory.setHelper(appContext);



    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }

    public static Context getAppContext() {
        return applicationContext;
    }
}
