package at.gleb.yachampion

import at.gleb.yachampion.models.Day
import at.gleb.yachampion.models.Note
import at.gleb.yachampion.stats_screen.StatsPresenter
import java.util.*

/**
 * Интерфейсы, определяющие взаимодействие View и Presenter   для разныхх экранов
 * Created by klgleb on 28/06/16.
 */


//базовые интерфейсы
interface BaseView<T> {
    var presenter: T
}

interface BasePresenter {
    fun start()//Можно показывать данные и тд
}


//Экран MainScreen
interface MainContract {
    interface View : BaseView<Presenter> {

        var currentDate:Date
        fun showScreen(screenId: Int)

        var dateTitle: String
        var notes: ArrayList<Note>
        var type: String

        fun showAddDialog()
        fun showDatePickerDialog()


    }

    interface Presenter : BasePresenter {
        fun screen(screenId: Int)
        fun addClick()
        var selectedDate: Date
        fun selectDay(year: Int, monthOfYear: Int, dayOfMonth: Int)
        fun selectDayClick()
        fun currentDayClick()
    }
}


interface DayContract {
    interface View : BaseView<DayContract.Presenter> {
        var notes: ArrayList<Note>?

        var tName: String
        var tNames: ArrayList<Day>?

        fun showChange(item: Note)
        fun showRemove(item: Note)
        fun showTest()
    }

    interface Presenter : BasePresenter {
        val selectedDate: Date
        fun beginChange(item: Note)
        fun beginRemove(item: Note)
        fun change(item: Note, count: Int)
        fun remove(item: Note)
        fun stop()
        fun saveTName(s: String)
    }
}

interface DetectContract {
    interface View : BaseView<DetectContract.Presenter> {

        var detecting: Boolean
        var detectCount: Int
    }

    interface Presenter : BasePresenter {
        fun startDetect(startCount: Int)
        fun result(res: Int)

        fun stopDetect()
        var selectedDate: Date
    }
}

interface StatsContract {
    interface View : BaseView<StatsContract.Presenter> {
        var statistic: ArrayList<StatsPresenter.MyAdapterItem>?
    }

    interface Presenter : BasePresenter {

    }
}