package at.gleb.yachampion

/**
 * Интерфейс, описывающий стратегию присоединения фрагмента к главной activity
 *
 * Created by gleb on 03.08.16.
 */
interface AttacheableFragment {
    fun getTitleRes(): Int

    fun getMenuRes(): Int
}