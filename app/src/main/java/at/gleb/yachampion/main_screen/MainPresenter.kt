package at.gleb.yachampion.main_screen;

import at.gleb.yachampion.MainContract
import at.gleb.yachampion.R
import kt_extensions.MyAppCompatActivity
import kt_extensions.dateCurrent
import java.util.*

class MainPresenter(val view: MainContract.View, val apiActivity: MyAppCompatActivity)
: MainContract.Presenter {

    override var selectedDate: Date = dateCurrent()
        set(value) {
            field = value
        }

    init {
        view.presenter = this
    }


    override fun start() {
        view.showScreen(R.id.nav_tracker)
    }


    override fun screen(screenId: Int) {
        view.showScreen(screenId)
    }

    override fun addClick() {
        view.showAddDialog()
    }


    override fun selectDay(year: Int, monthOfYear: Int, dayOfMonth: Int) {

        val date = Date(year - 1900, monthOfYear, dayOfMonth)
        view.currentDate = date
    }

    override fun selectDayClick() {
        view.showDatePickerDialog()
    }

    override fun currentDayClick() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        selectDay(year, month, day)
    }


}