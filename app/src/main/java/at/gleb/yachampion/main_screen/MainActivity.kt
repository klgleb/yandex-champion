package at.gleb.yachampion.main_screen

import android.app.DatePickerDialog
import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import at.gleb.yachampion.AttacheableFragment
import at.gleb.yachampion.MainContract
import at.gleb.yachampion.R
import at.gleb.yachampion.day_screen.DayPresenter
import at.gleb.yachampion.day_screen.instanceDayFragment
import at.gleb.yachampion.detect_scr.DetectFragment
import at.gleb.yachampion.detect_scr.DetectPresenter
import at.gleb.yachampion.models.Note
import at.gleb.yachampion.stats_screen.StatsPresenter
import at.gleb.yachampion.stats_screen.instanceStatsFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kt_extensions.MyAppCompatActivity
import kt_extensions.d
import kt_extensions.dateCurrent
import java.util.*

class MainActivity : MyAppCompatActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        MainContract.View {
    override var dateTitle: String = ""
        set(value) {
        }
    override var notes: ArrayList<Note> = arrayListOf()
        set(value) {
        }
    override var type: String = ""
        set(value) {
        }


    override var currentDate: Date = dateCurrent()
        set(value) {
            field = value
            dayPresenter?.selectedDate = value
            detectPresenter?.selectedDate = value

            presenter.selectedDate = value
            d("set current date")

        }

    var menuRes: Int = R.menu.main //меню по-умолчанию

    override lateinit var presenter: MainContract.Presenter


    private var dayPresenter: DayPresenter? = null
    private var detectPresenter: DetectPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            presenter.addClick()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)


        presenter = MainPresenter(this, this)
        presenter.start()

    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        //noinspection SimplifiableIfStatement
        when (id) {
            R.id.action_select_day -> {
                presenter.selectDayClick()
                return true
            }

            R.id.action_current_day -> {
                presenter.currentDayClick()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    @SuppressWarnings("StatementWithEmptyBody")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        presenter.screen(item.itemId)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun showScreen(screenId: Int) {
        // Handle navigation view item clicks here.

        var fragment: Fragment? = null

        when (screenId) {
            R.id.nav_tracker -> {
                fragment = instanceDayFragment()
                dayPresenter = DayPresenter(fragment, this)
                dayPresenter?.selectedDate = currentDate
            }

            R.id.nav_statistic -> {
                fragment = instanceStatsFragment()
                StatsPresenter(fragment, this)
            }

        //creating fragment instance
        /*fragment = instanceRecentlyCloseFragment()
        RecentlyClosePresenter(fragment, this)*/

        }

        if (fragment != null) {
            showFragment(fragment)
        }

    }

    override fun showAddDialog() {
        val dialog = DetectFragment()
        detectPresenter = DetectPresenter(dialog, this)

        val selectedDate = presenter.selectedDate
        detectPresenter!!.selectedDate = selectedDate

        dialog.presenter = detectPresenter!!



        dialog.show(fragmentManager, "Tipa tag")
    }


    //Показ фрагмента
    private fun showFragment(fragment: Fragment?) {

        if (fragment != null) {

            fragmentManager.beginTransaction()
                    .replace(R.id.content, fragment)
                    .commit()

            val titleRes: Int

            if (fragment is AttacheableFragment) {
                titleRes = fragment.getTitleRes()
                menuRes = fragment.getMenuRes()
            } else {
                menuRes = R.menu.main
                titleRes = R.string.app_name
            }

            invalidateOptionsMenu()

            supportActionBar?.title = getString(titleRes)
        }


    }

    override fun showDatePickerDialog() {
        /*
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
*/


        var year = currentDate.year + 1900
        var month = currentDate.month
        var day = currentDate.date


        val listener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            presenter.selectDay(year, monthOfYear, dayOfMonth)
        }


        val dialog = DatePickerDialog(this, listener, year, month, day)

        dialog.show()


    }

}
