package at.gleb.yachampion.models.db


import at.gleb.yachampion.models.Day
import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.support.ConnectionSource
import at.gleb.yachampion.models.Note
import java.sql.SQLException

open class NotesDao @Throws(SQLException::class)
constructor(connectionSource: ConnectionSource,
            dataClass: Class<Note>) : BaseDaoImpl<Note, Int>(connectionSource, dataClass) {
}

open class DaysDao @Throws(SQLException::class)
constructor(connectionSource: ConnectionSource,
            dataClass: Class<Day>) : BaseDaoImpl<Day, Int>(connectionSource, dataClass) {
}