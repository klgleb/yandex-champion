package at.gleb.yachampion.models.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import at.gleb.yachampion.models.Day
import at.gleb.yachampion.models.Note
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import java.sql.SQLException
import kotlin.reflect.KProperty

/**
 * Класс DataBaseHelper, который будет отвечать за создание БД и за получение ссылок на DAO:
 *
 *
 * Created by klgleb on 02.11.15.
 */
class DatabaseHelper(context: Context) : OrmLiteSqliteOpenHelper(context, DatabaseHelper.Companion.DATABASE_NAME, null, DatabaseHelper.Companion.DATABASE_VERSION) {
    companion object {

        private val TAG = DatabaseHelper::class.java.simpleName

        //имя файла базы данных, который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
        public val DATABASE_NAME = "MyApp.db"

        //с каждым увеличением версии  при нахождении в устройстве БД с предыдущей версией
        // будет выполнен метод onUpgrade();
        private val DATABASE_VERSION = 34
    }


    //ссылки на DAO соответсвующие сущностям, хранимым в БД

    val notesDao: NotesDao? by GetDao {
        NotesDao(getConnectionSource(), Note::class.java)
    }

    val daysDao: DaysDao? by GetDao {
        DaysDao(getConnectionSource(), Day::class.java)
    }


    //Выполняется, когда файл с БД не найден на устройстве
    override fun onCreate(db: SQLiteDatabase, connectionSource: ConnectionSource) {
        try {
            TableUtils.createTable(connectionSource, Note::class.java)
            TableUtils.createTable(connectionSource, Day::class.java)
        } catch (e: SQLException) {
            Log.e(DatabaseHelper.Companion.TAG, "error creating DB " + DatabaseHelper.Companion.DATABASE_NAME)
            throw RuntimeException(e)
        }

    }

    //Выполняется, когда БД имеет версию отличную от текущей
    override fun onUpgrade(db: SQLiteDatabase, connectionSource: ConnectionSource, oldVer: Int,
                           newVer: Int) {
        try {
            //Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
            TableUtils.dropTable<Note, Any>(connectionSource, Note::class.java, true)
            TableUtils.dropTable<Day, Any>(connectionSource, Day::class.java, true)
            onCreate(db, connectionSource)
        } catch (e: SQLException) {
            Log.e(DatabaseHelper.Companion.TAG, "error upgrading db " + DatabaseHelper.Companion.DATABASE_NAME + "from ver " + oldVer)
            throw RuntimeException(e)
        }

    }


    //выполняется при закрытии приложения
    override fun close() {
        super.close()
    }


}


class GetDao<T>(val initializer: () -> T) {

    var v: T? = null

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        if (v != null) {
            return v
        } else {
            try {
                v = initializer()
            } catch (t: Throwable) {
                return null
            }

            return v
        }
    }
}