package at.gleb.yachampion.models

import android.content.Context
import at.gleb.yachampion.models.db.HelperFactory
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import kt_extensions.broadcast
import java.util.*

/**
 * Классы для данных (pojo)
 *
 * Created by klgleb on 02/07/16.
 */

open class Resp<T> {
    var data: Array<T?>? = null
}


@DatabaseTable(tableName = "notes")
class Note() {

    @DatabaseField(generatedId = true)
    var id: Int = 0

    @DatabaseField
    var day: Int = 0

    @DatabaseField
    var month: Int = 0

    @DatabaseField
    var year: Int = 0

    @DatabaseField
    var count: Int = 0
}

@DatabaseTable(tableName = "days")
class Day() {

    @DatabaseField(generatedId = true)
    var id: Int = 0

    @DatabaseField
    var day: Int = 0

    @DatabaseField
    var month: Int = 0

    @DatabaseField
    var year: Int = 0

    @DatabaseField
    var type: String = ""
}


fun Context.registerNewNote(date: Date, count: Int) {
    val note = Note()
    val d = date.date
    val m = date.month
    val y = date.year + 1900


    note.count = count
    note.day = d
    note.month = m
    note.year = y

    HelperFactory.getHelper().notesDao?.create(note)

    broadcast("UpdateNotes")
}

fun Context.getNotes(date: Date): ArrayList<Note>? {
    val d = date.date
    val m = date.month
    val y = date.year + 1900

    val hashMapOf = hashMapOf<String, Any>(
            "day" to d,
            "month" to m,
            "year" to y
    )



    return ArrayList(HelperFactory.getHelper().notesDao?.queryForFieldValues(hashMapOf))


}

fun Context.saveTName(date: Date, title: String) {

    val day = Day()
    val d = date.date
    val m = date.month
    val y = date.year + 1900


    day.day = d; day.month = m; day.year = y

    day.type = title



    val hashMapOf = hashMapOf<String, Any>(
            "day" to d,
            "month" to m,
            "year" to y
    )

    val daysDao = HelperFactory.getHelper().daysDao
    val c = daysDao?.queryForFieldValues(hashMapOf)
    if (c == null || c.size == 0) {

        daysDao?.create(day)
    } else {
        val day1 = c[0]

        day1.day = d; day1.month = m; day1.year = y
        day1.type = title

        daysDao?.update(day1)
    }




}

fun Context.getTName(date: Date): String {

    try {
        val d = date.date
        val m = date.month
        val y = date.year + 1900


        val hashMapOf = hashMapOf<String, Any>(
                "day" to d,
                "month" to m,
                "year" to y
        )

        val daysDao = HelperFactory.getHelper().daysDao
        val c = daysDao?.queryForFieldValues(hashMapOf)
        val day1 = c!![0]

        return day1.type
    } catch(e: Throwable) {
        return ""
    }

}

fun Context.getTNames(): ArrayList<Day> {
    return ArrayList(HelperFactory.getHelper().daysDao?.queryForAll())

}
