package at.gleb.yachampion.stats_screen;

import at.gleb.yachampion.StatsContract
import at.gleb.yachampion.models.db.HelperFactory
import kt_extensions.MyAppCompatActivity
import kt_extensions.doAsync
import java.util.*

/**
 * Presenter for Stats  that is involved to MVP, using StatsContract
 * <p>
 * @author Gleb Klimov,  info@gleb.at
 * @version 23.10.16
 */
class StatsPresenter(val view: StatsContract.View, val activity: MyAppCompatActivity)
: StatsContract.Presenter {


    init {
        view.presenter = this
    }


    override fun start() {
        doAsync({
            val notes = HelperFactory.getHelper().notesDao?.queryForAll()
            val days = HelperFactory.getHelper().daysDao?.queryForAll()

            val adapterData: ArrayList<MyAdapterItem> = arrayListOf()

            if (notes != null && days != null) {

                for (day in days) {
                    val notestForTheDay = notes.filter { it.day == day.day && it.month == day.month && it.year == day.year }

                    val title = day.type
                    val date = "${day.day}.${day.month}.${day.year}"
                    val map = notestForTheDay.map { it.count.toString() }
                    var descr = map.joinToString("+")
                    
                    descr = "${notestForTheDay.sumBy { it.count }}\n$descr"

                    adapterData.add(MyAdapterItem(title, date, descr))
                }
            }
            
            return@doAsync adapterData
        }, {
            view.statistic = it
            
        })
    }

    class MyAdapterItem(var title: String, var date: String, var descr: String) {

    }



}