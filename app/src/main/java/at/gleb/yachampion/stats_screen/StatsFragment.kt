package at.gleb.yachampion.stats_screen;

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import at.gleb.yachampion.R
import at.gleb.yachampion.StatsContract
import at.gleb.yachampion.custiom.DividerItemDecoration
import at.gleb.yachampion.models.Day
import kotlinx.android.synthetic.main.fragment_stats.view.*
import kotlinx.android.synthetic.main.stats_item.view.*
import kt_extensions.w
import java.util.*

/**
 * Fragment for Stats
 * Also it is a View that is involved to MVP, using StatsContract
 * @see StatsPresenter
 * <p>
 * @author Gleb Klimov,  info@gleb.at
 * @version 23.10.16
 */
class StatsFragment : Fragment(), StatsContract.View {

    override lateinit var presenter: StatsContract.Presenter

    var v: View? = null

    override var statistic: ArrayList<StatsPresenter.MyAdapterItem>? = null
        set(value) {
            if (value != null) {
                try {
                    (v?.recyclerView?.adapter as StatsAdapter).items = value
                } catch(e: Throwable) {
                    w(e)
                }
            }
        }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater?.inflate(R.layout.fragment_stats, container, false)
        v?.recyclerView?.layoutManager = LinearLayoutManager(activity)
        v?.recyclerView?.addItemDecoration(DividerItemDecoration(activity))
        v?.recyclerView?.adapter = StatsAdapter(activity, presenter)

        presenter.start()

        return v
    }


    class StatsAdapter (var context: Context?, var presenter: StatsContract.Presenter) :
            RecyclerView.Adapter<StatsAdapter.MyViewHolder>() {

        var items: ArrayList<StatsPresenter.MyAdapterItem> = arrayListOf()
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

            val view = LayoutInflater.from(parent?.context)
                    .inflate(R.layout.stats_item, parent, false)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

            val item = items[position]

            holder?.view?.dateText.text = "${item.date} ${item.title}"
            holder?.view?.descText.text = item.descr




            /*
            if (context != null) {
                try {
                    Glide.with(App.appContext).load(item.imgUrl).into(holder?.view?.imageView4)
                } catch(e: Exception) {
                    w(e)
                }
            }*/




        }

        override fun getItemCount(): Int {
            return items.size
        }


        class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)
    }
}

fun instanceStatsFragment(): StatsFragment {
    val fragment = StatsFragment()

    /*val args = Bundle()     
    fragment.arguments = args*/

    return fragment
}
