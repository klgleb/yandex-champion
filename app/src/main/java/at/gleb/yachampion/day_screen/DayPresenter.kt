package at.gleb.yachampion.day_screen;

import at.gleb.yachampion.DayContract
import at.gleb.yachampion.models.*
import at.gleb.yachampion.models.db.HelperFactory
import kt_extensions.MyAppCompatActivity
import kt_extensions.broadcast
import kt_extensions.dateCurrent
import kt_extensions.doAsync
import java.util.*

/**
 * Presenter for Day  that is involved to MVP, using DayContract
 * <p>
 * @author Gleb Klimov,  info@gleb.at
 * @version 18.10.16
 */
class DayPresenter(val view: DayContract.View, val activity: MyAppCompatActivity)
: DayContract.Presenter {

    override var selectedDate: Date = dateCurrent()
        set(value) {
            field = value
            updNotes()
        }

    init {
        view.presenter = this
    }


    override fun start() {
        updNotes()


        activity.receiver("UpdateNotes") {
            updNotes()

        }
    }

    private fun updNotes() {
        val notes = activity.getNotes(selectedDate)

        view.notes = notes
        /*view.tNames = notes*/
//        autoCompleteTextView

        view.tName = activity.getTName(selectedDate)
        view.tNames =  activity.getTNames()
    }

    override fun saveTName(s: String) {

        doAsync({activity.saveTName(selectedDate, s)})
    }

    override fun stop() {
    }


    override fun beginChange(item: Note) {
        view.showChange(item)
    }

    override fun beginRemove(item: Note) {
        view.showRemove(item)
    }

    override fun change(item: Note, count: Int) {
        item.count = count
        HelperFactory.getHelper().notesDao?.update(item)
        activity.broadcast("UpdateNotes")
    }

    override fun remove(item: Note) {
        HelperFactory.getHelper().notesDao?.delete(item)
        activity.broadcast("UpdateNotes")

    }

}