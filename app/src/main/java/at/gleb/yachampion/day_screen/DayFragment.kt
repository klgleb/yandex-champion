package at.gleb.yachampion.day_screen

import android.app.AlertDialog
import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import at.gleb.yachampion.DayContract
import at.gleb.yachampion.R
import at.gleb.yachampion.custiom.DividerItemDecoration
import at.gleb.yachampion.models.Day
import at.gleb.yachampion.models.Note
import kotlinx.android.synthetic.main.fragment_day.view.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.android.synthetic.main.note_item.view.*
import kt_extensions.pl
import kt_extensions.str
import kt_extensions.w
import java.util.*


/**
 * Fragment for Day
 * Also it is a View that is involved to MVP, using DayContract
 * @see DayPresenter
 * <p>
 * @author Gleb Klimov,  info@gleb.at
 * @version 18.10.16
 */
class DayFragment : Fragment(), DayContract.View {
    override var tName: String = ""
        set(value) {
            v?.autoCompleteTextView?.setText(value)
        }

    override var tNames: ArrayList<Day>? = null
        set(value) {
            if (value != null) {
                try {
                    var days = value.map(Day::type)

//                    val hashSet = HashSet(days.filter { it.trim() != "" })//.toTypedArray()

                    val hashSet = HashSet<String>()
                    hashSet.addAll(days)
                    days = arrayListOf()
                    days.addAll(hashSet)

                    days = days.filter { it.trim() != "" }

                    val adapter = ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, days)
                    v?.autoCompleteTextView?.setAdapter(adapter)
                } catch(e: Throwable) {
                    w(e)
                }
            }
        }

    override lateinit var presenter: DayContract.Presenter

    var v: View? = null


    override var notes: ArrayList<Note>? = null
        set(value) {
            val dayAdapter = v?.recyclerView?.adapter as DayAdapter?

            if (value != null) {
                dayAdapter?.items = value
            }
        }

    init {
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater?.inflate(R.layout.fragment_day, container, false)

        v?.recyclerView?.layoutManager = LinearLayoutManager(activity)
        v?.recyclerView?.addItemDecoration(DividerItemDecoration(activity))

        val dayAdapter = DayAdapter(activity, presenter)

        v?.recyclerView?.adapter = dayAdapter
        dayAdapter.items = arrayListOf(
        )

        v?.autoCompleteTextView?.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {


            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                presenter.saveTName(s.toString())
            }
        })

        presenter.start()






        return v
    }

    override fun showTest() {

    }

    override fun showChange(item: Note) {
        var m_Text = ""

        val builder = AlertDialog.Builder(activity)
        builder.setTitle(str(R.string.detect_title))

        val input = EditText(activity)
        input.inputType = InputType.TYPE_CLASS_NUMBER
        input.setText(item.count.toString())
        builder.setView(input)

// Set up the buttons
        builder.setPositiveButton(R.string.ok) { dialog, which ->
            m_Text = input.text.toString()

            val toInt = try {
                input.text.toString().toInt()
            } catch (t: Throwable) {
                0
            }


            presenter.change(item, toInt)

            dialog.cancel()
        }

        builder.setNegativeButton(R.string.cancel) { dialog, which -> dialog.cancel() }

        builder.show()
    }

    override fun showRemove(item: Note) {

    }


    override fun onDestroyView() {
        super.onDestroyView()
        presenter.stop()

    }
}

fun instanceDayFragment(): DayFragment {
    val fragment = DayFragment()

    /*val args = Bundle()     
    fragment.arguments = args*/

    return fragment
}


class DayAdapter(var context: Context?, var presenter: DayContract.Presenter) :
        RecyclerView.Adapter<DayAdapter.MyViewHolder>() {

    var items: ArrayList<Note> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.note_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item = items[position]

        //holder?.view?.titleText?.text = item.title


        holder.view.textView2.text =
                context?.pl(item.count, R.plurals.squatting) ?: item.count.toString()


        /*
        if (context != null) {
            try {
                Glide.with(App.appContext).load(item.imgUrl).into(holder?.view?.imageView4)
            } catch(e: Exception) {
                w(e)
            }
        }*/


        holder?.view?.setOnClickListener {
            presenter.beginChange(item)
        }


        holder?.view?.deleteButton?.setOnClickListener {
//            presenter.beginRemove(item)
            presenter.remove(item)
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }


    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)


}