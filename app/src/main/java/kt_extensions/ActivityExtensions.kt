package kt_extensions

import android.app.Dialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.KeyEvent
import at.gleb.yachampion.R


/**
 * Kotlin extension functions for Activities.
 *
 * Created by klgleb on 13/04/16.
 */

fun FragmentActivity.addFragment(fragment: Fragment, frameId: Int) {
    val transaction = this.supportFragmentManager.beginTransaction()
    transaction.add(frameId, fragment)
    transaction.commit()
}

fun FragmentActivity.showProgressDialog() {

    hideProgressDialog()

    val ft = supportFragmentManager.beginTransaction()
    val prev = supportFragmentManager.findFragmentByTag("showProgressDialog")
    if (prev != null) {
        ft.remove(prev)
    }
    ft.addToBackStack(null)
    // Create and show the dialog.
    val newFragment = newProgressDialog()
    newFragment.show(ft, "showProgressDialog")
}

fun FragmentActivity.hideProgressDialog() {
    val prev = supportFragmentManager.findFragmentByTag("showProgressDialog")
    if (prev != null) {
        (prev as ProgressDialogFragment)?.dismiss()
    }
}


class ProgressDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = ProgressDialog(activity)
        dialog.setMessage(getString(R.string.loading_text))
        dialog.isIndeterminate = true
        dialog.setCancelable(false)

        // Disable the back button
        val keyListener = DialogInterface.OnKeyListener { dialog, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                return@OnKeyListener true
            }
            false
        }
        dialog.setOnKeyListener(keyListener)
        return dialog
    }

}

fun newProgressDialog(): ProgressDialogFragment {
    val frag = ProgressDialogFragment()
    return frag
}