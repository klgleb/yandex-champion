package kt_extensions

import java.util.*


fun Int.dateFrotTimestamp(): Date {
    val i:Long = (this * 1000).toLong()
    return Date(i)
}