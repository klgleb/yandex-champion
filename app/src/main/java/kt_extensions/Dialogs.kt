package kt_extensions

import android.app.AlertDialog
import android.app.Fragment
import android.content.Context
import android.content.DialogInterface
import android.text.InputType
import android.widget.EditText

/**
 * Created by Gleb Klimov, info@gleb.at
 * on 19.10.16.
 */


fun Context.inputNumber(
        title:String,
        inputType:Int =InputType.TYPE_CLASS_NUMBER,
        callback:(s:String)->Unit
        ){
    var m_Text = ""

    val builder = AlertDialog.Builder(this)
    builder.setTitle(title)

// Set up the input
    val input = EditText(this)
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
    input.inputType = inputType
    builder.setView(input)

// Set up the buttons
    builder.setPositiveButton("OK") { dialog, which ->
        m_Text = input.text.toString()
        callback(m_Text)
        dialog.cancel()
    }

    builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

    builder.show()
}


fun Fragment.inputNumber(
        title:String,
        inputType:Int =InputType.TYPE_CLASS_NUMBER,
        callback:(s:String)->Unit
) = activity.inputNumber(title, inputType, callback)