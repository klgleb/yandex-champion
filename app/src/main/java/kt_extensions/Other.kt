package kt_extensions

/**
 * Created by klgleb on 04/05/16.
 */

//Location
/*

fun Location.toLatLng(): LatLng {
    return LatLng(this.latitude, this.longitude)
}

fun LatLng.toLocation(): Location {
    val location = Location("point")
    location.latitude = this.latitude
    location.longitude = this.longitude
    return location
}

*/

fun String.isEmail(): Boolean {
    val ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
    val p = java.util.regex.Pattern.compile(ePattern)
    val m = p.matcher(this)
    return m.matches()
}

