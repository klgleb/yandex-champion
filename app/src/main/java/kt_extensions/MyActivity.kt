package kt_extensions

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import java.util.*

open class MyAppCompatActivity : AppCompatActivity() {
    val MY_PERMISSIONS_REQUEST = 7
    var receivers: Array<BroadcastReceiver> = arrayOf()

    private var afterFineLocation: ArrayList<() -> Unit> = arrayListOf()


    /**
     * Геолокация начинает автоматически работать при передаче onLocationChanged.
     */
    var onLocationChanged: ((location: Location) -> Unit)? = null
        set(value) {
            if (locationManager == null) {
                locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            }

            if (value != null) {
                afterFineLocation {
                    locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListener)
                }
            } else {
                if (isFineLocation()) {
                    locationManager?.removeUpdates(locationListener)
                }

            }

            field = value
        }

    private var locationManager: LocationManager? = null

    fun receiver(action: String, f: () -> Unit) {
        val broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                f()
            }

        }

        receiver(action, broadcastReceiver)
    }

    fun receiver(action: String, r: BroadcastReceiver) {

        receivers += r

        val intentFilter = IntentFilter()
        intentFilter.addAction(action)
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(r, intentFilter);
    }

    override fun onDestroy() {
        super.onPause()

        for (receiver in receivers) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        }
    }

    fun afterFineLocation(f: () -> Unit) {
        afterFineLocation(f, false)
    }

    fun afterFineLocation(required: Boolean, f: () -> Unit) {
        afterFineLocation(f, required)
    }

    fun afterFineLocation(f: () -> Unit, required: Boolean = false) {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (!required && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                toast("Unfortunately, the application does not have permission for this action")

            } else {
                afterFineLocation.add(f)

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST);
            }
        } else {
            f()
        }
    }

    fun isFineLocation(): Boolean {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                for (f in afterFineLocation) {
                    f()
                }

                afterFineLocation = arrayListOf()
            } else {
                afterFineLocation = arrayListOf()
            }



            for (perm in afterPermissionsList) {

                var accept = true
                for (p in perm.permission) {
                    if (!isPermission(p)) {
                        accept = false
                    }
                }

                if (accept) {
                    perm.after()
                } else {
                    perm.disagree()
                }
            }

            afterPermissionsList = arrayListOf()
        }
    }

    override fun onResume() {
        if (locationManager == null && onLocationChanged != null) {
            locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        }

        if (onLocationChanged != null) {
            if (isFineLocation()) {
                locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListener)
            }
        }

        super.onResume()
    }

    override fun onPause() {
        if (onLocationChanged != null && isFineLocation()) {
            locationManager?.removeUpdates(locationListener)
        }

        super.onPause()
    }

    private var locationListener = object : LocationListener {
        override fun onProviderDisabled(provider: String?) {

        }

        override fun onProviderEnabled(provider: String?) {

        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        }

        override fun onLocationChanged(location: Location?) {
            if (location != null && onLocationChanged != null) {
                this@MyAppCompatActivity.onLocationChanged?.invoke(location)
            }
        }

    }


    //permissions

    private var afterPermissionsList = arrayListOf<PermObject>()

    class PermObject(val permission: Array<String>, val after: () -> Unit, val disagree: () -> Unit)


    //Manifest.permission.ACCESS_FINE_LOCATION
    fun isPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this,
                permission) == PackageManager.PERMISSION_GRANTED
    }

    fun afterPermissions(permission: Array<String>, after: () -> Unit, disagree: () -> Unit = {}) {

        var need = false
        for (p in permission) {
            if (!isPermission(p)) {
                need = true
            }
        }

        if (!need) {
            after()
        } else {
            afterPermissionsList.add(PermObject(permission, after, disagree))

            ActivityCompat.requestPermissions(this,
                    permission,
                    MY_PERMISSIONS_REQUEST);
        }
    }
}