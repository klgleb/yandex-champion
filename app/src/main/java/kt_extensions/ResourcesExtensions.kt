package kt_extensions

import android.app.Fragment
import android.content.Context

/**
 * Created by Gleb Klimov, info@gleb.at
 * on 19.10.16.
 */


fun Context.pl(count: Int, resId:Int):String  =
    this.resources.getQuantityString(resId, count, count)



fun Context.str(resId: Int) = getString(resId)

fun Fragment.str(resId: Int) = activity.str(resId)