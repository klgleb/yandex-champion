package kt_extensions

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import java.util.*

/**
 *  Kotlin extension functions for Context.
 *
 * Created by klgleb on 29.02.16.
 */

fun Context.toast(text: String?, duration: Int = Toast.LENGTH_SHORT) {
    if (text != null) {
        Toast.makeText(this, text, duration).show()
    }
}

fun Context.toast(resId:Int, duration: Int = Toast.LENGTH_SHORT){
    val s = getString(resId)
    toast(s, duration)
}


fun d(message: String?, tag: String = "MyTag") {
    Log.d(tag, message ?: "null")
}

fun w(message: String? = "", throwable: Throwable? = null, tag: String = "MyTag") {
    if (throwable == null) {
        Log.w(tag, message ?: "null")
    } else {
        Log.w(tag, throwable)
    }

}

fun w(throwable: Throwable? = null, message: String? = null) {
    w(message, throwable = throwable)
}

fun Context.broadcast(action: String, extras: Bundle = Bundle(), local: Boolean = true) {
    var intent = Intent(action)
    intent.putExtras(extras)

    if (local) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    } else {
        sendBroadcast(intent)
    }

}

fun <Result> doAsync(background: () -> Result, ui: (f: Result) -> Unit = {}) {
    object : AsyncTask<Void, Void, Result>() {
        override fun doInBackground(vararg params: Void): Result {
            return background()
        }

        override fun onPostExecute(result: Result) {
            super.onPostExecute(result)

            ui(result)
        }
    }.execute()
}


fun Context.isMyServiceRunning(serviceClass: Class<*>): Boolean {
    val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
        if (serviceClass.name == service.service.getClassName()) {
            return true
        }
    }
    return false
}


fun rnd(a: Int, b: Int):Int {
    val r = Random()
    val i1 = r.nextInt(b - a) + a
    return i1
}