package kt_extensions

import android.support.v4.app.Fragment
import android.widget.Toast


fun Fragment.toast(text: String?, duration: Int = Toast.LENGTH_SHORT) {
    activity.toast(text, duration)
}