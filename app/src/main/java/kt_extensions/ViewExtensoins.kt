package kt_extensions

import android.view.View

fun View.visible(b: Boolean) {
    if (b) {
        visibility = View.VISIBLE
    } else {
        visibility = View.INVISIBLE
    }
}
