package kt_extensions

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.net.ParseException
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentActivity
import android.text.TextUtils
import android.widget.DatePicker
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun Date.format1(context: Context): String {

    val dateFormat: DateFormat

    val format = Settings.System.getString(context.contentResolver, Settings.System.DATE_FORMAT)
    if (TextUtils.isEmpty(format)) {
        dateFormat = android.text.format.DateFormat.getMediumDateFormat(context)
    } else {
        dateFormat = SimpleDateFormat(format)
    }

    try {
        return dateFormat.format(this)
    } catch (e: ParseException) {
        e.printStackTrace()

        return ""
    }


}

fun Date.toUnixTimestamp(): Long {
    return this.time / 1000
}

fun dateFrom(year: Int, month: Int, day: Int): Date {
    val cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month);
    cal.set(Calendar.DAY_OF_MONTH, day);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    return cal.time;
}

fun dateFromTimestamp(timestamp: Long): Date {
    return Date(timestamp * 1000)
}

fun Long.toDate():Date {
    return dateFromTimestamp(this)
}


fun dateCurrent(): Date {
    return Date(System.currentTimeMillis());
}


fun showDatePickerDialog(activity: FragmentActivity, currentDate: Date? = null, callback: ((Date) -> Unit)? = null) {
    val newFragment = DatePickerFragment()

    var date = currentDate

    val calendar = Calendar.getInstance()

    if (date == null) {
        date = dateCurrent()
    }

    calendar.time = date


    val args = Bundle()
    args.putInt("m", calendar.get(Calendar.MONTH))
    args.putInt("d", calendar.get(Calendar.DAY_OF_MONTH))
    args.putInt("y", calendar.get(Calendar.YEAR))

    newFragment.arguments = args
    newFragment.callBack = callback

    newFragment.show(activity.supportFragmentManager, "datePicker");
}

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    var callBack: ((Date) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val m = arguments.getInt("m")
        val d = arguments.getInt("d")
        val y = arguments.getInt("y")

        return DatePickerDialog(activity, this, y, m, d)

    }

    override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val date = dateFrom(year, monthOfYear, dayOfMonth)
        callBack?.invoke(date)
    }
}